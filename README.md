# serveur-stats-maladie

Serveur permettant d'obtenir des graphiques correspondant au pourcentage de patient par maladie et la répartition des maladies par tranche d'âge.

## Dépendances
Le serveur est écris en java avec l'api vert.x 4 (pour le serveur), jfreechart (pour la création des graphiques), jfreesvg (pour la conversion des graphiques en svg) ainsi que le connecteur Java fourni par mariadb pour la communication avec la base de donnée. Le serveur est compatible avec Java 8 mais Java 11 est fortement recommandé notamment car les dernières versions de jfreesvg ne sont plus compatibles avec java 8. Il est possible d'adapter facilement l'application pour d'autres type de base de donnée.

## Sécurité
Le serveur utilise un certificat auto-signé et prend en charge la communication en HTTPS. Toute connexion en HTTP sera redirigé vers une connexion en HTTPS. Le serveur est compatible avec TLS 1.2 et 1.3 uniquement.

Lors du démarrage du serveur, il vous sera demandé d'entrer l'adresse du serveur de base de donné, un nom d'utilisateur et un mot de passe. Cela permet d'éviter d'avoir le couple identifiant/mot de passe dans le code de l'application.

Vous pouvez retrouver un utilisateur exemple sur le projet dédié à la base de donnée.

## Api
Chaque API retourne un fichier svg comme résultat.

L'api qui permet d'obtenir le pourcentage de patient par maladie est accessible depuis l'adresse `https://localhost:8071/api/maladie` en utilisant la méthode get ou post. Vous pouvez utiliser le paramètre (query) guerris pour ajouter ou non les patients guéris.

L'api qui permet d'obtenir la répartition des maladies par tranche d'âge est accessible depuis l'adresse `https://localhost:8071/api/repartition` en utilisant la méthode get ou post. Si vous souhaitez, vous pouvez obtenir le graphique pour un tranche d'âge spécifique avec le paramètre tranche ou depuis `https://localhost:8071/api/repartition/TrancheDemandé` qui représente la dizaine de la tranche d'âge. Par exemple, obtenir le graphique pour la tranche d'âge de 20 à 29 ans, il faut utiliser le paramètre tranche égal à 2. Il est aussi possible de créer sa propre tranche d'âge avec les paramètres min et max.

Chaque api s'actualise avec la base de données toute les 5 minutes. Chaque fichier svg sont généré à chaque actualisation.

## Contexte

Ce projet est un test technique demandé par samdoc pour ma demande de stage. Ce projet utilise une base de donnée que vous pouvez trouvez sur la page du groupe.
