package serveurmaladies.donnees;

import java.awt.Rectangle;
import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Scanner;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.graphics2d.svg.SVGGraphics2D;
import org.jfree.graphics2d.svg.SVGUtils;

import serveurmaladies.domaine.Mariadb_BDD;

public class MaladieService {

	private HashMap<Maladie, Double> list = new HashMap<Maladie, Double>();
	private final Mariadb_BDD connexion;
	private final Thread actualisation;
	private boolean stop = false;
	private HashMap<Maladie, Double> listGueris = new HashMap<Maladie, Double>();
	//En milliseconde
	private static final long actualisationTime = 300000;

	public MaladieService() {
		boolean connecter = false;
		String login, mdp;
		Console console = System.console();
		Mariadb_BDD tempConnexion = null;
		if(console==null)
			System.err.println("Impossible d'ouvrir la console, pour une meilleur sécurité. Cela peut arriver si vous executez le programme sous eclipse");
	    System.out.println("Veuillez rentrer l'adresse de la base de données (laissez vide pour localhost)");
		Scanner sc = new Scanner(System.in);
	    String adresse = sc.nextLine();
	    if(adresse==null||adresse.isEmpty())
	    	adresse="localhost";
		do {
			try {
				System.out.println("Veuillez rentrer votre identifiant pour vous connecter à la base de données");
				login = sc.nextLine();
				System.out.println("Veuillez rentrer votre mot de passe pour vous connecter à la base de données");
				if(console!=null)
					mdp = new String(console.readPassword());
				else
					mdp = sc.nextLine();
				tempConnexion  = new Mariadb_BDD(adresse, login, mdp);
				connecter=true;
			}catch(SQLException e) {
				System.out.println("Erreur lors de la connexion "+e);
			}
		}while(!connecter);
		connexion = tempConnexion;
		sc.close();
		actualisation = new Thread(() -> {
			try {
				try {
					Files.createDirectories(Paths.get("assets/"));
				} catch (IOException e) {
				}
				while(!stop) {
					System.out.println("Actualisation des données");
					initList();
					System.out.println("Actualisation des graphiques");
					createChartMaladies();
					for(int i=0;i<10;i++)
						createChartTranche(i);
					Thread.sleep(actualisationTime);
				}
			}catch(InterruptedException e) {
				
			}
			
		}); 
		actualisation.start();
	}
	private void initList() {
		list = connexion.getRepartitionMaladie(false);
		listGueris = connexion.getRepartitionMaladie(true);
	}
	public void close() {
		stop = true;
		actualisation.interrupt();
	}
	@SuppressWarnings("unchecked")
	public void createChartMaladies() {
		DefaultPieDataset<Maladie> pieDataset = new DefaultPieDataset<Maladie>();
		for(Maladie i:list.keySet()) {
			pieDataset.setValue(i, list.get(i));
		}
		JFreeChart pieChart = ChartFactory.createPieChart(null, pieDataset, true, true, true);
		PiePlot<Maladie> plot = (PiePlot<Maladie>) pieChart.getPlot();
        plot.setSimpleLabels(false);

        PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator("{0} ({2})", new DecimalFormat("0"), new DecimalFormat("0.00%"));
        plot.setLabelGenerator(gen);
		SVGGraphics2D g2 = new SVGGraphics2D(550, 500);
		Rectangle r = new Rectangle(0, 0, 550, 500);
		pieChart.draw(g2, r);
		File f = new File("assets/latest.svg"), f2=new File("assets/latestALL.svg");;
		try{
			f.delete();
			f2.delete();
			SVGUtils.writeToSVG(f, g2.getSVGElement());
		} catch (SecurityException|IOException e) {
			e.printStackTrace();
		}
		for(Maladie i:listGueris.keySet()) {
			pieDataset.setValue(i, listGueris.get(i));
		}
		pieChart = ChartFactory.createPieChart(null, pieDataset, true, true, true);
		plot = (PiePlot<Maladie>) pieChart.getPlot();
        plot.setSimpleLabels(true);
        plot.setLabelGenerator(gen);
		g2 = new SVGGraphics2D(550, 500);
		r = new Rectangle(0, 0, 550, 500);
		pieChart.draw(g2, r);
		try{
			SVGUtils.writeToSVG(f2, g2.getSVGElement());
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	public void createChartTranche(int tranche) {
		if(tranche<=9&&tranche>=0) {
			DefaultCategoryDataset data = new DefaultCategoryDataset();
			HashMap<String, Double> hm;
			String nomTranche;
			if(tranche==9)
				nomTranche="90 et +";
			else
				nomTranche=tranche*10+"-"+((tranche+1)*10-1);
			hm = connexion.getRepartitionTranche(tranche);
			for(String j:hm.keySet()) {
				data.setValue(hm.get(j).doubleValue()*100, j, nomTranche);
			}
			final JFreeChart chart = ChartFactory.createBarChart(null, "Tranche d'âge", "Pourcentage de patients par maladie", data, PlotOrientation.VERTICAL, true, true, true);
			CategoryPlot plot = chart.getCategoryPlot();
			NumberAxis rangeAxis = (NumberAxis)plot.getRangeAxis();
			rangeAxis.setRange(0, 100);
			SVGGraphics2D g2 = new SVGGraphics2D(400, 600);
			Rectangle r = new Rectangle(0, 0, 400, 600);
			chart.draw(g2, r);
			File f = new File("assets/repartition_"+tranche+".svg");
			try {
				f.delete();
			} catch (SecurityException e1) {
			}
			try{
				SVGUtils.writeToSVG(f, g2.getSVGElement());
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
	public void createChartTranche(int min, int max) {
		if(min>=0) {
			DefaultCategoryDataset data = new DefaultCategoryDataset();
			HashMap<String, Double> hm;
			String nomTranche;
			if(max==-1)
				nomTranche=min+" et +";
			else
				nomTranche=min+"-"+max;
			hm = connexion.getRepartitionTranche(min, max);
			for(String j:hm.keySet()) {
				data.setValue(hm.get(j).doubleValue()*100, j, nomTranche);
			}
			final JFreeChart chart = ChartFactory.createBarChart(null, "Tranche d'âge", "Pourcentage de patients par maladie", data, PlotOrientation.VERTICAL, true, true, true);
			CategoryPlot plot = chart.getCategoryPlot();
			NumberAxis rangeAxis = (NumberAxis)plot.getRangeAxis();
			rangeAxis.setRange(0, 100);
			SVGGraphics2D g2 = new SVGGraphics2D(400, 600);
			Rectangle r = new Rectangle(0, 0, 400, 600);
			chart.draw(g2, r);
			File f = new File("assets/repartition_"+min+"-"+max+".svg");
			try {
				f.delete();
			} catch (SecurityException e1) {
			}
			try{
				SVGUtils.writeToSVG(f, g2.getSVGElement());
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * L'api utilisé essaye de placer chaque maladie dans chaque tranche, ce qui cause des barres trop fines sur le graphique
	 */
	@Deprecated
	public void createChartTranche() {
		DefaultCategoryDataset data = new DefaultCategoryDataset();
		HashMap<String, Double> hm;
		String nomTranche;
		for(int i=0;i<10;i++) {
			if(i==9)
				nomTranche="90 et +";
			else
				nomTranche=i*10+"-"+((i+1)*10-1);
			hm = connexion.getRepartitionTranche(i);
			for(String j:hm.keySet()) {
				data.setValue(hm.get(j).doubleValue()*100, j, nomTranche);
			}
		}
		final JFreeChart chart = ChartFactory.createBarChart(null, "Tranche d'âge", "Pourcentage de patients par maladie", data, PlotOrientation.VERTICAL, true, true, false);
		CategoryPlot plot = chart.getCategoryPlot();
		BarRenderer br = (BarRenderer) plot.getRenderer();
		br.setItemMargin(0.05);
		NumberAxis rangeAxis = (NumberAxis)plot.getRangeAxis();
		rangeAxis.setRange(0, 100);
		SVGGraphics2D g2 = new SVGGraphics2D(800, 600);
		Rectangle r = new Rectangle(0, 0, 800, 600);
		chart.draw(g2, r);
		File f = new File("assets/repartition.svg");
		try {
			f.delete();
		} catch (SecurityException e1) {
		}
		try{
			//OutputStream out = new FileOutputStream("assets/latest.png");
			//ChartUtils.writeChartAsPNG(out,pieChart, 1000, 1000);
			SVGUtils.writeToSVG(f, g2.getSVGElement());
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}
