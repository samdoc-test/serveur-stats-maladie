package serveurmaladies.donnees;

public class Maladie implements Comparable<Maladie> {
	private final int idMaladie;
	private final String nomMaladie;
	public Maladie(int idMaladie, String nomMaladie) {
		this.nomMaladie = nomMaladie;
		this.idMaladie = idMaladie;
	}
	public String getNomMaladie() {
		return nomMaladie;
	}
	public int getIdMaladie() {
		return idMaladie;
	}
	@Override
	public String toString() {
		return nomMaladie;
	}
	@Override
	public int compareTo(Maladie o) {
		return o instanceof Maladie ? idMaladie-((Maladie) o).getIdMaladie() :null;
	}
}
