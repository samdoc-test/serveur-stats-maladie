package serveurmaladies.domaine;

import java.sql.SQLException;

public class Mariadb_BDD extends BDD {
	private static final String piloteMariadb = "org.mariadb.jdbc.Driver";
	private static final String chaineDeConnexionMariadbBegin = "jdbc:mysql://";
	private static final String chaineDeConnexionMariadbEnd = "/dbSuivi";
	public Mariadb_BDD(String adresse, String login, String mdp) throws SQLException {
		super(login, mdp, piloteMariadb, new StringBuilder(chaineDeConnexionMariadbBegin).append(adresse).append(chaineDeConnexionMariadbEnd).toString());
        connecter();
	}

}
