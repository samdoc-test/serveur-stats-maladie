package serveurmaladies.domaine;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import serveurmaladies.donnees.Maladie;

public abstract class BDD {
	
	private ResultSet curseurJava;
    private Statement objReq;
    private final String chaineConnexion;
    private final String login;
    private final String mdPasse;
    private Connection conn;
    private final String pilote;
	
	public BDD(String login, String mdPasse, String pilote, String chaineConnexion) {
		this.login=login;
		this.mdPasse=mdPasse;
		this.pilote=pilote;
		this.chaineConnexion=chaineConnexion;
	}
    public void connecter() throws SQLException{
    	try {
    		Class.forName(pilote);
			conn = DriverManager.getConnection(chaineConnexion,login,mdPasse);
			DatabaseMetaData metaData = (DatabaseMetaData) conn.getMetaData();
			System.out.println("Connexion réussie à "+metaData.getDatabaseProductName());
			objReq = conn.createStatement();
    	}catch(ClassNotFoundException e) {
    		System.err.println(" Erreur de chargement du driver :" + e);
    	}
    }
	public HashMap<Maladie, Double> getRepartitionMaladie(boolean guerris) {
		HashMap<Maladie, Double> hashMap = new HashMap<Maladie, Double>();
		final String req = "select MA_ID, MA_NOM, count(*)/(select count(*) from MALADIE_ATTRAPEE where MAAT_GUERRIS="+Boolean.toString(guerris)+") as pourcentage from MALADIE_ATTRAPEE join MALADIE using(MA_ID) where MAAT_GUERRIS="+Boolean.toString(guerris)+" group by MA_ID";
		try {
    		curseurJava = objReq.executeQuery(req);
	    	while (curseurJava.next()) {
	    		hashMap.put(new Maladie(curseurJava.getInt(1), curseurJava.getString(2)), curseurJava.getDouble(3));
	    	}
	    	return hashMap;
    	}
    	catch(SQLException ex) {
	    	System.err.println (ex.getErrorCode());
	    	System.err.println (ex.getMessage());
	    	System.err.println("Erreur : "+ex);
	    	return null;
    	}
	}
    public HashMap<String, Double> getRepartitionTranche(int min, int max) {
    	HashMap<String, Double> hashMap = new HashMap<String, Double>();
    	final String req;
    	if(max!=-1) {
			req = "select MA_ID, MA_NOM, count(*)/(select count(*) from MALADIE_ATTRAPEE join PATIENT using(PA_ID) where floor(DATEDIFF(sysdate(), PA_DATE_NAISSANCE)/365) between "+min+" and "+max+") as pourcentage from MALADIE_ATTRAPEE join MALADIE using(MA_ID) join PATIENT using(PA_ID) where floor(DATEDIFF(sysdate(), PA_DATE_NAISSANCE)/365) between "+min+" and "+max+" group by MA_ID";
    	}else {
			req = "select MA_ID, MA_NOM, count(*)/(select count(*) from MALADIE_ATTRAPEE join PATIENT using(PA_ID) where floor(DATEDIFF(sysdate(), PA_DATE_NAISSANCE)/365) >= "+min+") as pourcentage from MALADIE_ATTRAPEE join MALADIE using(MA_ID) join PATIENT using(PA_ID) where floor(DATEDIFF(sysdate(), PA_DATE_NAISSANCE)/365) >= "+min+" group by MA_ID";
    	}
    	
    	try {
			curseurJava = objReq.executeQuery(req);
	    	while (curseurJava.next()) {
	    		hashMap.put(curseurJava.getString(2), curseurJava.getDouble(3));
	    	}
	    	return hashMap;
    	}
    	catch(SQLException ex) {
	    	System.err.println (ex.getErrorCode());
	    	System.err.println (ex.getMessage());
	    	System.err.println("Erreur : "+ex);
	    	return null;
    	}
    }
    public HashMap<String, Double> getRepartitionTranche(int tranche) {
    	return getRepartitionTranche(tranche*10,tranche>=9?-1:(tranche+1)*10-1);
    }
}
