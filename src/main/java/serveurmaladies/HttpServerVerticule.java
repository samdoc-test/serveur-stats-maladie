package serveurmaladies;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;

public class HttpServerVerticule extends AbstractVerticle {
	public HttpServerVerticule() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public void start() throws Exception{
		HttpServer server = vertx.createHttpServer();
		server.requestHandler(r->{
			r.response().setStatusCode(301).putHeader("Location", r.absoluteURI().replace("http", "https")).end();
		}).listen(config().getInteger("http.port", MainServeur.port));
	}
}
