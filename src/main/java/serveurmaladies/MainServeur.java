package serveurmaladies;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import serveurmaladies.donnees.MaladieService;

public class MainServeur extends AbstractVerticle{
	
	public static final int port = 8071;
	private static final MaladieService serviceMaladie = new MaladieService();
	
	@Override
	public void start() throws Exception{
		vertx.deployVerticle(HttpsServerVerticule.class.getCanonicalName());
		vertx.deployVerticle(HttpServerVerticule.class.getCanonicalName(), new DeploymentOptions().setInstances(1));
	}
	public static final MaladieService getServiceMaladie() {
		return serviceMaladie;
	}
}
