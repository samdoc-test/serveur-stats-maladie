package serveurmaladies;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.net.SelfSignedCertificate;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import serveurmaladies.donnees.MaladieService;

public class HttpsServerVerticule extends AbstractVerticle{
	
	private static final String contentType = "content-type";
	private static final String contentTypeHTML = "text/html; charset=utf-8";
	private static final String incorrectValue = "Incorrect value";
	private static final String apiMaladiePath = "/api/maladie";
	private static final String apiRepartitionPath = "/api/repartition";
	private static final String apiRepartitionPathWithParams = "/api/repartition/:tranche";
	private static final String assetsParams = "/assets/:fileName";
	private static final String fileName = "fileName";
	private static final String assets = "assets/";
	private static final String homePath = "/";
	private static final String titleRepartition = "<h1>Répartion des maladies par tranche d'âge</h1>";
	private static final String beginRepartition = "<img src='/assets/repartition_";
	private static final String endRepartition = ".svg' alt='Graphique'/><br/>";
	private static final String contentTypeSVG = "image/svg+xml";
	private static final String tranche = "tranche";
	private static final String latest = "assets/latest";
	private static final String svg = ".svg";
	private static final String min = "min";
	private static final String max = "max";
	private static final String repartition = "repartition_";
	private static final String guerris = "guerris";
	
	private MaladieService serviceMaladie;
	
	public void start() {
		serviceMaladie = MainServeur.getServiceMaladie();
		Router router = Router.router(vertx);
		System.out.println("Création de l'handler "+homePath);
		router.route(homePath).handler(routingContext -> {
			HttpServerResponse response = routingContext.response();
			response.putHeader(contentType, contentTypeHTML)
			.end("<h1>Hello from my first Vert.x application</h1>");
		});

		System.out.println("Création de l'handler "+apiMaladiePath);
		router.get(apiMaladiePath).handler(this::getGrapheMaladie);
		router.post(apiMaladiePath).handler(this::getGrapheMaladie);
		System.out.println("Création de l'handler "+apiRepartitionPath);
		router.get(apiRepartitionPath).handler(this::getGrapheRepartition);
		router.post(apiRepartitionPath).handler(this::getGrapheRepartition);
		System.out.println("Création de l'handler "+apiRepartitionPathWithParams);
		router.get(apiRepartitionPathWithParams).handler(this::getGrapheRepartition);
		router.post(apiRepartitionPathWithParams).handler(this::getGrapheRepartition);
		router.route(assetsParams).handler(routingContext -> {
			routingContext.response().sendFile(assets+routingContext.request().params().get(fileName));
		});
		System.out.println("Création du certificat TLS");
		SelfSignedCertificate certificate = SelfSignedCertificate.create();
		HttpServerOptions secureOptions = new HttpServerOptions().
			 	setTcpKeepAlive(true).
				setSsl(true).
				removeEnabledSecureTransportProtocol("TLSv1").
				removeEnabledSecureTransportProtocol("TLSv1.1").
				addEnabledSecureTransportProtocol("TLSv1.2").
				addEnabledSecureTransportProtocol("TLSv1.3").
				setKeyCertOptions(certificate.keyCertOptions()).
				setTrustOptions(certificate.trustOptions());
		
		System.out.println("Lancement de l'écoute sur le port "+MainServeur.port);
		vertx.createHttpServer(secureOptions).requestHandler(router).listen(config().getInteger("http.port", MainServeur.port));
	}
	@Override
	public void stop() throws Exception {
		System.out.println("Serveur arrété");
	}
	public void getGrapheMaladie(final RoutingContext routingContext) {
		MultiMap multi = routingContext.request().params();
		if(multi.contains(guerris)) {
			boolean guerrisBool = Boolean.parseBoolean(multi.get(guerris));
			StringBuilder sb = new StringBuilder(latest);
			if(guerrisBool) {
				sb.append("all");
			}
			sb.append(svg);
			routingContext.response().
			putHeader(contentType, contentTypeSVG).
			sendFile(sb.toString());
		}else {
			routingContext.response()
			.putHeader(contentType, contentTypeSVG)
			.sendFile(latest+svg);
		}
		
	}
	public void getGrapheRepartition(final RoutingContext routingContext) {
		MultiMap multi = routingContext.request().params();
		if(multi.isEmpty()||(!multi.contains(tranche)&&!multi.contains(min)&&!multi.contains(max))) {
			StringBuilder sb = new StringBuilder(titleRepartition);
			for(int i=0;i<10;i++) {
				sb.append(beginRepartition);
				sb.append(i);
				sb.append(endRepartition);
			}
			routingContext.response().
			putHeader(contentType, contentTypeHTML).
			end(sb.toString());
		}
		else {
			try {
				if(multi.contains(tranche)) {
					int trancheInt = Integer.parseInt(multi.get(tranche));
					if(trancheInt>9||trancheInt<0)
						throw new NumberFormatException();
					serviceMaladie.createChartTranche(trancheInt);
					routingContext.response().
					putHeader(contentType, contentTypeSVG).
					sendFile(assets+repartition+trancheInt+svg);
				}else{
					final int minInt, maxInt;
					if(multi.contains(min)) {
						minInt = Integer.parseInt(multi.get(min));
						if(minInt<0)
							throw new NumberFormatException();
						if(multi.contains(max))
							maxInt = Integer.parseInt(multi.get(max));
						else
							maxInt=-1;
					}else {
						minInt=-1;
						maxInt = Integer.parseInt(multi.get(max));
						if(maxInt<0)
							throw new NumberFormatException();
					}
					serviceMaladie.createChartTranche(minInt,maxInt);
					routingContext.response().
					putHeader(contentType, contentTypeSVG).
					sendFile(assets+repartition+minInt+"-"+maxInt+svg);
				}
			}catch(NumberFormatException e) {
				serviceMaladie.createChartTranche(Integer.parseInt(multi.get(tranche)));
				routingContext.response()
				.putHeader(contentType, contentTypeHTML)
				.end(incorrectValue);				
			}
		}
	}

}
